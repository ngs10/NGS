



const Viewpage = ({ formDeta, photoSrc}) => {


    return (
        <>


            <div className="viewpage">
                <div className="heading-style">
                    <h3>{formDeta.StudentFirstName}</h3>
                    <span>Registration ID. {formDeta.regno}</span><br />
                    <span>Expiry Date {formDeta.exDate}</span>
                </div>
                <div className="flex_contain">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="profile_view">
                                <figure>{photoSrc && (<img src={photoSrc} alt={photoSrc} />)}</figure>
                                {formDeta.StudentFirstName && (<>
                                    <h3 className="bg-title">Personal Detail</h3>
                                    <div className="rtl_view p0">
                                        <h4>Full Name: {formDeta.StudentFirstName} {formDeta.StudentLastName}</h4>
                                        <h4>Gender: {formDeta.StudentGender}</h4>
                                        <h4>Date of Birth: {formDeta.StudentDob}</h4>
                                        <h4>B-From: {formDeta.StudentBForm}</h4>
                                        {formDeta.StudentEmail && (<h4>Email ID: {formDeta.StudentEmail}</h4>)}
                                        {formDeta.GuardianAddress && (<p>Address: {formDeta.GuardianAddress}</p>)}
                                        {formDeta.StudentInfomation && (<p>Remarks: {formDeta.StudentInfomation}</p>)}
                                    </div>
                                </>
                                )}
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="rtl_view">
                                {formDeta.StudentEntryTestType && (
                                    <>
                                        <h3 className="bg-title">Entry Test Detail</h3>
                                        <h4>Test Type : {formDeta.StudentEntryTestType}</h4>
                                        <h4>Test Date : {formDeta.StudentEntryTestDate}</h4>
                                    </>
                                )}
                                {formDeta.StudentClassPlan && (
                                    <>
                                        <h3 className="bg-title">Student Class Plan</h3>
                                        <h4>Class Plan : {formDeta.StudentClassPlan}</h4>
                                        <ul className="sub_list"><li className="none-bg"><h4>Subjects:</h4></li> { formDeta.StudentSubjects.map((val,i)=> <li key={i}>{val}</li> )}</ul>
                                        {formDeta.StudentSession && (<h4>Session : {formDeta.StudentSession}</h4>)}
                                    </>
                                )}
                                {formDeta.diseasename && (
                                    <>
                                        <h3 className="bg-title">Medical information</h3>
                                        {formDeta.diseasename && (<h4>Disease Name : {formDeta.diseasename}</h4>)}
                                        {formDeta.medicalcondition && (<h4>Medical Condition: {formDeta.medicalcondition}</h4>)}
                                        {formDeta.emergencynumber && (<h4>Emergency Number: {formDeta.emergencynumber}</h4>)}
                                        {formDeta.mentalhealth && (<h4>Mental Health: {formDeta.mentalhealth}</h4>)}
                                    </>
                                )}

                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="rtl_view">
                                {formDeta.GuardianName && (<>
                                    <h3 className="bg-title">Primary Guardian Detail</h3>
                                    <h4>Name : {formDeta.GuardianName}</h4>
                                    {formDeta.GuardianEmail && (<h4>Email@ : {formDeta.GuardianEmail}</h4>)}
                                    <h4>CNIC Number : {formDeta.GuardianCNIC}</h4>
                                    <h4>Phone number : {formDeta.GuardianPhone}</h4>
                                    {formDeta.GuardianOccupation && (<h4>Guardian Occupation : {formDeta.GuardianOccupation}</h4>)}
                                    {formDeta.GuardianRelation && (<h4>Guardian Relation : {formDeta.GuardianRelation}</h4>)}
                                    <p>Address: {formDeta.GuardianAddress}</p>
                                    {formDeta.AddmissionOfficerReviewsP && (<p>Remarks: {formDeta.AddmissionOfficerReviewsP}</p>)}
                                </>)
                                }
                                {formDeta.SecondguardianName && (<>
                                    <h3 className="bg-title">Secondary Guardian Detail</h3>
                                    <h4>Name : {formDeta.SecondguardianName}</h4>
                                    <h4>Email@ : {formDeta.SecondguardianEmail}</h4>
                                    <h4>CNIC Number : {formDeta.SecondguardianCNIC}</h4>
                                    <h4>Phone number : {formDeta.SecondguardianPhone}</h4>
                                    {formDeta.SecondguardianOccupation && (<h4>Guardian Occupation : {formDeta.SecondguardianOccupation}</h4>)}
                                    {formDeta.SecondguardianRelation && (<h4>Guardian Relation : {formDeta.SecondguardianRelation}</h4>)}
                                    <p>Address: {formDeta.SecondguardianAddress}</p>
                                    {formDeta.AddmissionOfficerReviewsS && (<p>Remarks: {formDeta.AddmissionOfficerReviewsS}</p>)}
                                </>)
                                }
                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </>
    )

}
export default Viewpage;