import React from 'react';
import Students from '../Componets/Students';
import Api from "../Studentlist.json";
function StudentsList() {
    return (
        <div className='flex'>
            {Api.map((val)=>{
                return(
                <Students
                    id={val.id}
                    key={val.id}
                    img={val.img}
                    alt={val.alt}
                    name={val.name}
                    email={val.email}
                    address={val.address.street}
                    phone={val.phone}
                />
                );
            })}
            
        </div>    
        
    );
}

export default StudentsList;