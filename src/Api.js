




import axios from 'axios';

// const apiUrl = 'http://localhost:8080/api/';
export const Registration = async (data, options) => {
    try {
        await axios.post(process.env.NGS_APIURL + 'Registration/NewRegistration?LoginId=1&BrId=1', data, options);
    } catch (error) {
        throw error;
    }
}


export const singleFileUpload = async (data, options) => {
    try {
        await axios.post(process.env.NGS_APIURL + 'Registration/NewRegistration?LoginId=1&BrId=1', data, options);
    } catch (error) {
        throw error;
    }
}
export const getSingleFiles = async () => {
    try {
            const {data} = await axios.get(process.env.NGS_APIURL + 'upload');
            return data;
    } catch (error) {
        throw error;
    }
}

export const multipleFilesUpload = async (data, options) => {
    try {
        await axios.post(process.env.NGS_APIURL + 'upload', data, options);
    } catch (error) {
        throw error;
    }
}
export const getMultipleFiles = async () => {
    try{
        const {data} = await axios.get(process.env.NGS_APIURL + 'upload');
        return data;
    }catch(error){
        throw error;
    }
}