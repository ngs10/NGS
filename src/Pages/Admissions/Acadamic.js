

import Input from '../../Componets/widgets/ValidationInputs/input';
function Acadamic({ Arry, checked, handleCheck, Ischecked, formDeta }) {


    return (
        <div className='flex_grid'>
            {Arry.map((val, index) => {
                return (
                    <div className='level_one' key={index}>
                        <h5>{val.name}</h5>
                        {val.terms.map((vel, i) => {
                            return (
                                <div key={i} className='level_one'>
                                    {vel.termName && <h5>{vel.termName}</h5>}
                                    <div className='flex'>
                                        <div className='flex columns'>
                                            <h6 className='sm-title'>{vel.CompulsorySubjects.name}</h6>
                                            {vel.CompulsorySubjects.subject.map((sb) => {
                                                return (
                                                    // console.log(sub)
                                                    <div className='columns' key={sb.sb}>
                                                        <Input
                                                            id={sb.id}
                                                            onChange={handleCheck}
                                                            type="checkbox"
                                                            cls="checkbox "
                                                            checked={checked}
                                                            fieldname={sb.sb}
                                                            span={<span></span>}
                                                            name="abc"
                                                            value={sb.sb}
                                                        />
                                                    </div>
                                                )
                                            })}
                                        </div>
                                        <div className='flex columns'>
                                            <h6 className='sm-title'>{vel.Elective.name}</h6>
                                            {vel.Elective.subject.map((sb) => {
                                                return (
                                                    <div className='columns' key={sb.id}>
                                                        <Input
                                                            id={sb.id}
                                                            type="checkbox"
                                                            cls="checkbox"
                                                            checked={checked}
                                                            fieldname={sb.sb}
                                                            onChange={handleCheck}
                                                            span={<span></span>}
                                                            name="abc"
                                                            value={sb.sb}
                                                        />
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                )
            })}

        </div>
    );
}

export default Acadamic;