import Cards from "../SubWidgets/card";

const Statistics = () => {
    const Arry = [
        {
            class: "status_cards bg-perple",
            title: "TOTAL STRENGTH",
            count: "195",
            iconName: "ti-harddrive",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-red",
            title: "CURRENT SESSION ADMISSION",
            count: "205",
            iconName: "ti-harddrive",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-skyblue",
            title: "PREVIOUS SESSION STRENGTH",
            count: "0",
            iconName: "dripicons-blog",
            trendingClass: "mdi mdi-trending-down rd",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-yellow",
            title: "LEFT CURRENT SESSION",
            count: "10",
            iconName: "dripicons-duplicate",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-green",
            title: "THIS MONTH ADMISSIONS",
            count: "500",
            iconName: "dripicons-graduation",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-perple",
            title: "THIS MONTH LEFT",
            count: "0",
            iconName: "ti-harddrive",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-red",
            title: "FEE PENDING STUDENTS",
            count: "26",
            iconName: "ti-harddrive",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-skyblue",
            title: "FEE RECEIVED STUDENTS",
            count: "0",
            iconName: "dripicons-blog",
            trendingClass: "mdi mdi-trending-down rd",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-perple",
            title: "FEE RECEIVED STUDENTS",
            count: "0",
            iconName: "ti-harddrive",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
       
        {
            class: "status_cards bg-skyblue",
            title: "TOTAL FEE RECEIVED (Rs.)",
            count: "86150",
            iconName: "dripicons-blog",
            trendingClass: "mdi mdi-trending-down rd",
            percentage: "8.5%",
        },
        {
            class: "status_cards bg-red",
            title: "TOTAL FEE RECEIVABLE (Rs.)",
            count: "86150",
            iconName: "ti-harddrive",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
         {
            class: "status_cards bg-red",
            title: "NOT CLEARED CHEQUES FEE",
            count: "0",
            iconName: "ti-harddrive",
            trendingClass: "mdi mdi-trending-up",
            percentage: "8.5%",
        },
        
    ]
    return (
        <>
            <div className="deshboard_status">
                {
                    Arry.map((val,index) =>
                        <Cards
                            key={index}
                            id={index}
                            count={val.count}
                            classCard={val.class}
                            title={val.title }
                            iconName={ val.iconName}
                            trendingclassName={val.trendingClass}
                            percentage={val.percentage}
                        />
                    )
                }

            </div>
        </>
    );

}
export default Statistics;