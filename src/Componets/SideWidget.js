



/* eslint-disable no-unreachable */

import { useState } from "react";
import { Link } from "react-router-dom";
import Footer from "../layouts/Footer";


const Sidemenu = () => {
    const navigation = [
        {
            id: 1,
            title: "Deshboard",
            link: "/",
            icon: "ti-dashboard",
            badge: "new",
        },
        {
            id: 2,
            title: "Students",
            link: "",
            icon: "ti-dashboard",
            badge: "new",
            children: [
                {
                    id: 1,
                    title: "Registrations",
                    link: "/Registrationlist",
                    icon: "ti-dashboard",
                    badge: "new",
                },
                {
                    id: 2,
                    title: "Admissions",
                    link: "/Admissionslist",
                    icon: "ti-dashboard",
                    badge: "new",
                },
            ]
        },
        // {
        //     id: 3,
        //     title: "New Registration",
        //     link: "/Registrations",
        //     icon: "ti-bar-chart",
        //     badge: "new",
        // },
        // {
        //     id: 4,
        //     title: "Registrated Students",
        //     link: "/RegistratedStudentslist",
        //     icon: "ti-bar-chart",
        //     badge: "",
        // },
        // {
        //     id: 5,
        //     title: "New Admission",
        //     link: "/newAdmission",
        //     icon: "ti-bar-chart",
        //     badge: "new",
        // },
        // {
        //     id: 6,
        //     title: "Admissions list",
        //     link: "/Admissionslist",
        //     icon: "ti-bar-chart",
        //     badge: "new",
        // },
        // {
        //     id: 7,
        //     title: "All Students",
        //     link: "/student",
        //     icon: "ti-dashboard",
        //     badge: "",
        // },
        // {
        //     id: 8,
        //     title: "File Menager",
        //     link: "/filemaneger",
        //     icon: "ti-bar-chart",
        //     badge: "",
        // },
    ]
    const [activeId, setActiveId] = useState();
    const [childactive, setChildActive] = useState();
    return (
        <div className="side_wraper">

            <div className="sidebar">
                <nav>
                    {/* {val.children.map((subchild) => {
                        return (subchild.map((child) => <li key={child.id} className={activeId >= child.id ? "active" : !activeId >= child.id ? "active" : ""}>
                            <Link to={child.link} ><i className={child.icon}></i>{child.title}{child.badge && (<span className="badge">{child.badge}</span>)}</Link></li>)
                        )
                    })
                    }) */}
                    <ul className="left-sidenav">
                        {navigation.map((val) => {
                            return (
                                <li key={val.id} onClick={() => setActiveId(val.id)} className={activeId === val.id ? "active" : !activeId === val.id ? "active" : ""}><Link
                                    to={val.link} ><i className={val.icon}></i>{val.title}{val.badge && (<span className="badge">{val.badge}</span>)}</Link>
                                    {val.children && (<ul className={activeId === val.id ? "children active" : "children" }>
                                        {
                                            val.children.map((child)=>{
                                                return(
                                                <li key={child.id} onClick={() => setChildActive(child.id)} className={childactive === child.id ? "active" : !activeId === child.id ? "active" : ""}><Link to={child.link}>{child.title}</Link></li>
                                                )
                                            })
                                        }
                                        
                                    </ul>)}
                                </li>
                            )
                        })}

                    </ul>

                </nav>

            </div>
            <Footer />
        </div>

    );

}
export default Sidemenu;