/* eslint-disable no-mixed-operators */

const Textarea = (props) => {
    
    return (
        // size="4" maxlength="3"
        <>

            <div className={'custom-field fullwidth' && props.cls || "custom-field fullwidth"}>
                <textarea autoComplete="off" required type={props.type} onChange={props.onChange} disabled={props.disabled} name={props.name} id={props.fieldname} value={props.value} placeholder={props.placeholder}/>
                <label htmlFor={props.fieldname}><i className={`fa` && `fa ${props.icon}` || ""}></i><h6 className="margin0">{props.span}{props.fieldname}</h6></label>
                
            </div>
        </>


    )

}
export default Textarea;




