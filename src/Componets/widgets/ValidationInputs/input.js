

const Input = (props) => {
    
    return (
        // size="4" maxlength="3"
        <>

            <div className={'custom-field' && props.cls ? props.cls : "custom-field"}>
                <input defaultChecked={props.defaultChecked} autoComplete="off" required type={props.type} onChange={props.onChange} onClick={props.onClick} disabled={props.disabled} name={props.name} id={props.value} value={props.value} placeholder={props.fieldname} checked={props.checked}/>
                <label htmlFor={props.value}><i className={`fa` && `fa ${props.icon}` ? `fa ${props.icon}` : `fa ${props.icon}`}></i><h6 className="margin0">{props.span}{props.fieldname}</h6></label>
                
            </div>
            
        </>
                    

    )

}
export default Input;




