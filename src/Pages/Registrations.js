import axios from "axios";
import React, { useState, useCallback, useEffect } from "react";

import Registraion from "./Registrations/Registration";
import Fees from "./Registrations/Fees";
import Voucher from "./Registrations/voucher";

const Registrations = () => {

    const [page, setPage] = useState(0);
    const StepsTitles = ['Step 1', 'Step 2', 'Step 3'];
    const [Snapcapture, setSnapcapture] = useState('Take Picture');
    const [photoSrc, setPhotoSrc] = useState(null);
    const [stest, setStest] = useState(0);



    /////////////////////////////////////
    // This File Uploading Funcation ////
    /////////////////////////////////////


    /////File Var
    const [SBF, setSBF] = useState(null);
    const [PCNICF, setPCNICF] = useState(null);
    const [PCNICB, setPCNICB] = useState(null);
    const [LeavingCertificate, setLeavingCertificate] = useState(null);
    const [birthCertificate, setBirthCertificate] = useState(null);

    /////File SRC
    const [srcSBF, setSrcSBF] = useState(null);
    const [srcPCNICF, setSrcPCNICF] = useState(null);
    const [srcPCNICB, setSrcPCNICB] = useState(null);
    const [srcLeavingCertificate, setSrcLeavingCertificate] = useState(null);
    const [srcbirthCertificate, setSrcBirthCertificate] = useState(null);

    ///Image to Display
    const [imgDisplay, setImgDisplay] = useState(null);


    function GetBase64(img, callback) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }
    function Base64toFile(base64, FileName) {
        var arr = base64.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);

        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], FileName, { type: mime });
    }

    const onFileChange = (e) => {
        if (e.target.files[0].size > 1000000) {
            alert('Your File Size is more then 2MB Please take another file');
            return;
        }
        if (e.target.name === 'SBF') {
            setSBF(e.target.files[0]);
            GetBase64(e.target.files[0], setSrcSBF);
        }
        else if (e.target.name === 'PCNICF') {
            setPCNICF(e.target.files[0]);
            GetBase64(e.target.files[0], setSrcPCNICF);
        }
        else if (e.target.name === 'PCNICB') {
            setPCNICB(e.target.files[0]);
            GetBase64(e.target.files[0], setSrcPCNICB);
        }
        else if (e.target.name === 'BC') {
            setBirthCertificate(e.target.files[0]);
            GetBase64(e.target.files[0], setSrcBirthCertificate);
        }
        else if (e.target.name === 'LC') {
            setLeavingCertificate(e.target.files[0]);
            GetBase64(e.target.files[0], setSrcLeavingCertificate);
        }
    };

    const displayImg = useCallback((e) => {
        if (e.target.id === 'SBF') {
            setImgDisplay(srcSBF);
        }
        else if (e.target.id === 'PCNICF') {
            setImgDisplay(srcPCNICF);

        }
        else if (e.target.id === 'PCNICB') {
            setImgDisplay(srcPCNICB);
        }
        else if (e.target.id === 'BC') {
            setImgDisplay(srcbirthCertificate);
        }
        else if (e.target.id === 'LC') {
            setImgDisplay(srcLeavingCertificate);
        }
    }, [srcLeavingCertificate, srcPCNICB, srcPCNICF, srcSBF, srcbirthCertificate]);



    const ImageRemover = useCallback((id) => {
        if (id === 1) { setSrcSBF(undefined); }
        else if (id === 2) setSrcPCNICF(undefined);
        else if (id === 3) setSrcPCNICB(undefined);
        else if (id === 4) setSrcLeavingCertificate(undefined);
        else if (id === 5) setSrcBirthCertificate(undefined);
    }, []);



    const [GetDeta, setGetDeta] = useState();

    const getAPi = () => {
        try {
            // const data = await axios.post(`http://172.17.0.15:8001/api/Registration/NewRegistration?LoginId=${LoginId}&BrId=${BrId}`)
            axios.get(`http://172.17.0.15:8001/api/Registration/NewRegistration?LoginId=1&BrId=1`, {
                headers: {
                    'Content-type': 'application/json'
                }
            }).then((responce)=>{
                
                const regdeta = responce.data.registration.regStd.regNo;

                setGetDeta(regdeta);
                console.log(regdeta)
            })
            
            
            // console.log(res);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getAPi();
    }, []);
    // console.log(GetDeta)

    function Submitform(event) {
        event.preventDefault();
        axios({
            method: 'post',
            // URL: process.env.NGSAPP_NGS_APIURL + `Registration/NewRegistration?LoginId=${LoginId}&BrId=${BrId}`,
            URL: "asds",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            data: {
                regno: [formDeta.regno],
                regdate: "",
                exDate: "",
                studentImage: [formDeta.photoSrc],
                StudentPicture: [formDeta.photoSrc],
                StudentFirstName: [formDeta.StudentFirstName],
                StudentLastName: [formDeta.StudentLastName],
                StudentEmail: [formDeta.StudentEmail],
                StudentBForm: [formDeta.StudentBForm],
                StudentDob: [formDeta.StudentDob],
                StudentGender: [formDeta.StudentGender],
                StudentEntryTest: [formDeta.StudentEntryTest],
                StudentEntryTestDate: [formDeta.StudentEntryTestDate],
                StudentClassPlan: [formDeta.StudentClassPlan],
                StudentSession: [formDeta.StudentSession],
                StudentInfomation: [formDeta.StudentInfomation],
                StudentAddmissionOfficerReviews: [formDeta.StudentAddmissionOfficerReviews],
                GuardianFirstName: [formDeta.GuardianFirstName],
                GuardianLastName: [formDeta.GuardianLastName],
                GuardianEmail: [formDeta.GuardianEmail],
                GuardianCNIC: [formDeta.GuardianCNIC],
                GuardianPhone: [formDeta.GuardianPhone],
                GuardianOccupation: [formDeta.GuardianOccupation],
                GuardianAddress: [formDeta.GuardianAddress],
                guardianRelation: [formDeta.guardianRelation],

                BrachId: [formDeta.BrachId],
                Brachcity: [formDeta.Brachcity],
                BrachArea: [formDeta.BrachArea],
                AddmissionOfficerReviews: [formDeta.AddmissionOfficerReviews],

                // Upload files
                UploadFiles: [formDeta.UploadFiles],
                Studentbform: [formDeta.Studentbform],
                pcnicFront: [formDeta.pcnicFront],
                pcnicBack: [formDeta.pcnicBack],
                leavingcertificate: [formDeta.leavingcertificate],
                birthcertificate: [formDeta.birthcertificate],

                registrationid: [formDeta.registrationid],
            },


        }).then(function (response) {
            console.log(response);
        }).catch(function (error) {
            console.log(error);
        });

    }

    const radioHandler = () => {
        setStest(stest);
    }
    //// Model 
    const [modal, setModal] = useState(false);

    const toggleModal = () => {
        setModal(!modal);
    };

    if (modal) {
        document.body.classList.add('active-modal')
    } else {
        document.body.classList.remove('active-modal')
    }


    // let currentDate = new Date();
    // let CDate = currentDate.toLocaleDateString();


    const [formDeta, setFormDeta] = useState({
        regno: "",
        Regdate: "",
        ExDate: "",
        studentImage: "",
        StudentPicture: "",
        StudentFirstName: "",
        StudentLastName: "",
        StudentEmail: "",
        StudentBForm: "",
        StudentDob: "",
        StudentGender: "",
        StudentEntryTestType: "",
        StudentEntryTestDate: "",
        StudentClassPlan: "",
        StudentSession: "",
        StudentInfomation: "",


        GuardianName: "",
        GuardianLastName: "",
        GuardianEmail: "",
        GuardianCNIC: "",
        GuardianPhone: "",
        GuardianOccupation: "",
        GuardianAddress: "",
        GuardianRelation: "",
        AddmissionOfficerReviewsP: "",

        BrachId: "",
        Brachcity: "",
        BrachArea: "",

        AddmissionOfficerReviews: "",

        // Upload files
        UploadFiles: "",
        Studentbform: { SBF },
        pcnicFront: { PCNICF },
        pcnicBack: { PCNICB },
        leavingcertificate: { LeavingCertificate },
        birthcertificate: { birthCertificate },

        registrationid: "",
    });

    console.log(formDeta);


    const cities = [
        {
            id: 1,
            value: "Lahore",
            title: "Lahore",
        },
        {
            id: 2,
            value: "Islamabad",
            title: "Islamabad",
        },
        {
            id: 3,
            value: "Other",
            title: "Other",
        },
    ];

    console.log(formDeta);


    const DisplaySteps = () => {
        if (page === 0) {
            return <Registraion

                GetDeta={GetDeta}
                Snapcapture={Snapcapture}
                setSnapcapture={setSnapcapture}
                photoSrc={photoSrc}
                setPhotoSrc={setPhotoSrc}
                radioHandler={radioHandler}
                setStest={setStest}
                stest={stest}
                modal={modal}
                setModal={setModal}
                toggleModal={toggleModal}
                formDeta={formDeta}
                setFormDeta={setFormDeta}
                cities={cities}
                // Upload file
                ImageRemover={ImageRemover}
                imgDisplay={imgDisplay}
                srcSBF={srcSBF}
                srcPCNICF={srcPCNICF}
                srcPCNICB={srcPCNICB}
                srcLeavingCertificate={srcLeavingCertificate}
                srcbirthCertificate={srcbirthCertificate}
                displayImg={displayImg}
                onFileChange={onFileChange}


            />
        }
        else if (page === 1) {
            return <Fees
                formDeta={formDeta}
                setFormDeta={setFormDeta}
                cities={cities}
            />
            // 

        } else {
            return <Voucher
                formDeta={formDeta}
                setFormDeta={setFormDeta}
                photoSrc={photoSrc}
            />
        }
    }
    // const emailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // const phoneformat = /^03\d{9}$/;
    // const nameFormat = /^[a-zA-Z\s]+$/;


    // const[error,setError] = useState(false);



    // const validatePhone = (value, setError) => {
    //     if (value.match(phoneformat)) {
    //         if (setError) setError(false);
    //         return true;
    //     } else {
    //         if (setError) setError(true);
    //         return false;
    //     }
    // };

    //  const validatePassword = (value, setError) => {
    //     if (value.length > 7) {
    //         if (setError) setError(false);
    //         return true;
    //     } else {
    //         if (setError) setError(true);
    //         return false;
    //     }
    // };

    //  const validateEmail = (value, setError) => {
    //     if (value.match(emailformat)) {
    //         if (setError) setError(false);
    //         return true;
    //     } else {
    //         if (setError) setError(true);
    //         return false;
    //     }
    // };

    return (
        <>
            <ol className="c-progress-steps">
                <li className={page === 0 ? "c-progress-steps__step current" : page >= 0 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Student information</span></li>
                <li className={page === 1 ? "c-progress-steps__step current" : page >= 1 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Fees information</span></li>
                <li className={page === 2 ? "c-progress-steps__step current" : page >= 2 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Print voucher</span></li>
            </ol>

            {DisplaySteps()}

            <div className={page === 2 ? "btn-floating" : "btn-floating"}>
                <button className="btn btn-normal" disabled={page === 0} onClick={() => { setPage((currPage) => currPage - 1) }}>Preview</button>
                {page === StepsTitles.length - 1 ? <button className="btn btn-normal" onClick={Submitform}>submit</button> : <button className="btn btn-normal"
                    onClick={() => {
                        if (page === StepsTitles.length - 1) {
                            setFormDeta(formDeta);
                            Submitform({});
                        } else {
                            setPage((currPage) => currPage + 1)
                        }
                    }
                    }
                >{page === StepsTitles.length - 1 ? "" : "Next"}</button>}


            </div>
        </>
    );
};

export default Registrations;