/* eslint-disable no-use-before-define */
/* eslint-disable react-hooks/exhaustive-deps */

import React, { useState, useEffect, useCallback } from 'react';
import Acadamic from "../Admissions/Acadamic";
const FormPage3 = ({ formDeta, setFormDeta, classPlan, Arry }) => {

  // Function for subjects selection //
 

  const [checked, setChecked] = useState(formDeta.StudentSubjects);
  const [Ischecked, setISChecked] = useState(false);

  const handleCheck = useCallback((event) => {
    
    var updatedList = [...checked];
    if (event.target.checked) {
      updatedList = [...checked, event.target.value]; 
    } else {
      updatedList.splice(checked.indexOf(event.target.value));
    }
    setChecked(updatedList);
    setISChecked({...Ischecked, StudentSubjects: Ischecked});
  });

  const checkedItems = checked.length
    ? checked.reduce((total, item) => {
      return total + ", " + item;
    })
    : "";

  useEffect(() => {
    setFormDeta({ ...formDeta, StudentSubjects: checked })
  }, [checked])

  console.log(checkedItems)
  console.log(formDeta)

  return (
    <>
      <div className="heading mb20">
        <h4><img src={process.env.PUBLIC_URL + '/images/student.png'} alt="student" /><b>Acadamic information</b></h4>
      </div>

      <div className="row">
        <div className="col-12">
          <div className="flex form_desgin">

            <div className='flex'>

              <div className='columns'>

                <div className="custom-field selections_box">
                  <label><i className="fa fa-briefcase"></i><h6 className="margin0">Class Plan</h6></label>
                  <select onChange={(event) => setFormDeta({ ...formDeta, StudentClassPlan: event.target.value })} value={formDeta.StudentClassPlan}>
                    {classPlan && (
                      <>
                        {classPlan.map((val) => <option key={val.CLASS_ID} value={val.value}>{val.CLASS_Name}</option>)}
                      </>
                    )
                    }
                  </select>

                </div>
              </div>
            </div>


            {formDeta.StudentClassPlan === "1A" && (
              <Acadamic Arry={Arry} formDeta={formDeta} setFormDeta={setFormDeta} handleCheck={handleCheck} Ischecked={Ischecked} setISChecked={setISChecked} />
            )}

            {/* {`Items checked are: ${checkedItems}`} */}

          </div>
        </div>

      </div>
    </>
  )
}



export default FormPage3;