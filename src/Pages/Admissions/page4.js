/* eslint-disable react-hooks/exhaustive-deps */


const FormPage4 = ({ formDeta, setFormDeta }) => {

  

  return (
    <>
      <div className="heading mb20">
        <h4><img src={process.env.PUBLIC_URL + '/images/student.png'} alt="student" /><b>Fees information</b></h4>
      </div>

      <div className="row">
        <div className="col-12">
          <div className="flex form_desgin">
          <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>years</th>
                        <th>due date</th>
                        <th>Fees heads</th>
                        <th>Sub Total</th>
                        <th>Area</th>
                        <th>late payment</th>
                        <th>total paid Amount</th>
                        <th>payment late Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>shahzad Ali</td>
                        <td>shahzad@ngs.com</td>
                        <td>Ali</td>
                        <td>NGS School lahore</td>
                        <td>liberty</td>
                        <td>10000</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>zuhaib Ali</td>
                        <td>zuhaib@ngs.com</td>
                        <td>Hadi</td>
                        <td>NGS School lahore</td>
                        <td>liberty</td>
                        <td>10000</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>shahzad Ali</td>
                        <td>shahzad@ngs.com</td>
                        <td>Ali</td>
                        <td>NGS School lahore</td>
                        <td>liberty</td>
                        <td>10000</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>zuhaib Ali</td>
                        <td>zuhaib@ngs.com</td>
                        <td>Hadi</td>
                        <td>NGS School lahore</td>
                        <td>liberty</td>
                        <td>10000</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>shahzad Ali</td>
                        <td>shahzad@ngs.com</td>
                        <td>Ali</td>
                        <td>NGS School lahore</td>
                        <td>liberty</td>
                        <td>10000</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>zuhaib Ali</td>
                        <td>zuhaib@ngs.com</td>
                        <td>Hadi</td>
                        <td>NGS School lahore</td>
                        <td>liberty</td>
                        <td>10000</td>
                    </tr>
                </tbody>
            </table>
           
          </div>
        </div>

      </div>
    </>
  )
}



export default FormPage4;



// January	Jan.	31	winter
// 2	February	Feb.	28/29
// 3	March	Mar.	31	spring
// 4	April	Apr.	30
// 5	May	May	31
// 6	June	Jun.	30	summer
// 7	July	Jul.	31
// 8	August	Aug.	31
// 9	September	Sep.	30	autumn
// 10	October	Oct.	31
// 11	November	Nov.	30
// 12	December	Dec.	31	winter