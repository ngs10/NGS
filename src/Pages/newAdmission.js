
import { useState } from "react";
import axios from 'axios';
import FormPage from "./Admissions/page1";
import FormPage2 from "./Admissions/page2";
import Viewpage from './Admissions/Viewpage';
import FormPage3 from "./Admissions/page3";
import FormPage4 from "./Admissions/page4";


const ProForm = () => {

  const [Snapcapture, setSnapcapture] = useState('Take Picture');
  const [photoSrc, setPhotoSrc] = useState(null);




  const [page, setPage] = useState(0);

  const StepsTitles = ['Step 1', 'Step 2', 'Step 3', 'Step 4', 'Step 5'];


  let currentDate = new Date();
  let CDate = currentDate.toLocaleDateString();

  


  
  const [formDeta, setFormDeta] = useState({
    regno: "",
    regdate: "",
    exDate: "",
    studentImage: { photoSrc },
    StudentPicture: { photoSrc },
    StudentFirstName: "",
    StudentLastName: "",
    StudentEmail: "",
    StudentBForm: "",
    StudentDob: "",
    StudentGender: "male",
    StudentEntryTest: "",
    StudentEntryTestType: "",
    StudentEntryTestDate: "",
    StudentClassPlan: "",
    StudentSubjects: [],
    StudentFirstyr: '',
    StudentSecondyr: '',
    StudentSession: "",
    StudentInfomation: "",
    StudentReligion: "",
    StudentGrouping: "",
    StudentLanguage: "",
    // Medical info

    diseasename: "",
    medicalcondition: "",
    emergencynumber: "",
    mentalhealth: "",

    GuardianName: "",
    GuardianLastName: "",
    GuardianEmail: "",
    GuardianCNIC: "",
    GuardianPhone: "",
    GuardianOccupation: "",
    GuardianAddress: "",
    GuardianRelation: "",
    AddmissionOfficerReviewsP: "",

    SecondguardianName: "",
    SecondguardianLastName: "",
    SecondguardianEmail: "",
    SecondguardianCNIC: "",
    SecondguardianPhone: "",
    SecondguardianOccupation: "",
    Secondguardiancities: "",
    Secondguardiancitiesarea: "",
    SecondguardianAddress: "",
    SecondguardianRelation: "",
    AddmissionOfficerReviewsS: "",

    BrachId: "",
    Brachcity: "",
    BrachArea: "",

    AddmissionOfficerReviews: "",

    // Upload files
    UploadFiles: "",
    Studentbform: "",
    pcnicFront: "",
    pcnicBack: "",
    leavingcertificate: "",
    birthcertificate: "",

    registrationid: "",
  })



  const Arry = [
    {
      name: 'First Semester',
      terms: [
        {
          // termName: 'Mid Semester',

          CompulsorySubjects: {
            name: "Compulsory Subjects",
            subject: [
              {
                id: "0001",
                sb: "Urdu",
              },
              {
                id: "0002",
                sb: "English",
              },
              {
                id: "0003",
                sb: "Math",
              },
              {
                id: "0004",
                sb: "Science",
              },
            ]
          },
          Elective: {
            name: "Elective Subjects",
            subject: [
              {
                id: "0005",
                sb: "Physics",
              },
              {
                id: "0006",
                sb: "Computer",
              },
              {
                id: "0007",
                sb: "NetWorking",
              },
              {
                id: "0008",
                sb: "Algol",
              },
            ]
          }
        },
        {
          // termName: 'Final Semester',
          CompulsorySubjects: {
            name: "Compulsory Subjects",
            subject: [
              {
                id: "0009",
                sb: "Arabic",
              },
              {
                id: "0010",
                sb: "Islamiyat",
              },

            ]
          },
          Elective: {
            name: "Elective Subjects",
            subject: [
              {
                id: "0011",
                sb: "Criminology",
              },
              {
                id: "0012",
                sb: "Computer",
              },
              {
                id: "0013",
                sb: "Austrology",
              },
              {
                id: "0014",
                sb: "Algol",
              },

            ]
          }

        },
      ],
    },
    {
      name: 'Second Semester',
      terms: [
        {
          // termName: 'Mid Semester',
          CompulsorySubjects: {
            name: "Compulsory Subjects",
            subject: [
              {
                id: "0021",
                sb: "Urdu",
              },
              {
                id: "0022",
                sb: "English",
              },
              {
                id: "0023",
                sb: "Math",
              },
              {
                id: "0024",
                sb: "Science",
              },
            ]
          },
          Elective: {
            name: "Elective Subjects",
            subject: [
              {
                id: "0025",
                sb: "Physics",
              },
              {
                id: "0026",
                sb: "Computer",
              },
              {
                id: "0027",
                sb: "NetWorking",
              },
              {
                id: "0028",
                sb: "Algol",
              },
            ]

          }
        },
        {
          // termName: 'Final Semester',
          CompulsorySubjects: {
            name: "Compulsory Subjects",
            subject: [
              {
                id: "0029",
                sb: "Arabic",
              },
              {
                id: "0030",
                sb: "Islamiyat",
              },

            ]
          },
          Elective: {
            name: "Elective Subjects",
            subject: [
              {
                id: "0031",
                sb: "Criminology",
              },
              {
                id: "0032",
                sb: "Computer",
              },
              {
                id: "0033",
                sb: "Austrology",
              },


            ]
          }

        },
      ],
    }
  ]

  const classPlan = [
    {
      "CLASS_ID": 30271,
      "CLASS_Name": "Play Group (Red)",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "5nkk4As2LUs`idkFe`"
    },
    {
      "CLASS_ID": 30272,
      "CLASS_Name": "Reception (Red)",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "1`91R4ue`QbzKYlZDQ`idkFe`"
    },
    {
      "CLASS_ID": 30273,
      "CLASS_Name": "Preparatory (Red)",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "42ZfWYM01l0`idkFe`"
    },
    {
      "CLASS_ID": 30274,
      "CLASS_Name": "1A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "jR7wCw5A3dM`idkFe`"
    },
    {
      "CLASS_ID": 30275,
      "CLASS_Name": "2A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "Z16ufjFqsvM`idkFe`"
    },
    {
      "CLASS_ID": 30276,
      "CLASS_Name": "3A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "56ajS5oiKVs`idkFe`"
    },
    {
      "CLASS_ID": 30277,
      "CLASS_Name": "4A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "ltib`91R4ue`NMNqY4`idkFe`"
    },
    {
      "CLASS_ID": 30278,
      "CLASS_Name": "5A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "/Rx1ibXA6q0`idkFe`"
    },
    {
      "CLASS_ID": 30285,
      "CLASS_Name": "6A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "sbL5Nhmz`91R4ue`IU`idkFe`"
    },
    {
      "CLASS_ID": 30286,
      "CLASS_Name": "7A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "vFVBgIF8ufQ`idkFe`"
    },
    {
      "CLASS_ID": 30287,
      "CLASS_Name": "8A",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "2Q2DnpY2SV0`idkFe`"
    },
    {
      "CLASS_ID": 30288,
      "CLASS_Name": "O1R",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "DZd/Sg/GXlQ`idkFe`"
    },
    {
      "CLASS_ID": 30289,
      "CLASS_Name": "O2R",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "Ec2MZzMSD9g`idkFe`"
    },
    {
      "CLASS_ID": 30290,
      "CLASS_Name": "O3R",
      "CLASS_IS_SUPPLEMENTARY_BILLS": false,
      "EncryptId": "c0hdBYD8R2I`idkFe`"
    }
  ]

  const cities = [
    {
      id: 1,
      value: "Lahore",
      title: "Lahore",
    },
    {
      id: 2,
      value: "Islamabad",
      title: "Islamabad",
    },
    {
      id: 3,
      value: "Other",
      title: "Other",
    },
  ];




  const DisplaySteps = () => {
    if (page === 0) {
      return <FormPage
        formDeta={formDeta}
        setFormDeta={setFormDeta}
        cities={cities}
        photoSrc={photoSrc}
        setPhotoSrc={setPhotoSrc}
        setSnapcapture={setSnapcapture}
        Snapcapture={Snapcapture}
      />

    }
    else if (page === 1) {
      return <FormPage2
        formDeta={formDeta}
        setFormDeta={setFormDeta}
        cities={cities}

      />
    } else if (page === 2) {
      return <FormPage3
        formDeta={formDeta}
        setFormDeta={setFormDeta}
        cities={cities}
        classPlan={classPlan}
        Arry={Arry}

      />
    } else if (page === 3) {
      return <FormPage4
        formDeta={formDeta}
        setFormDeta={setFormDeta}

      />
    } else {
      return <Viewpage
        formDeta={formDeta}
        setFormDeta={setFormDeta}
        photoSrc={photoSrc}

      />
    }
  }





  function Submitform(e) {
    e.preventDefault();
    axios({
      method: 'Post',
      url: '/note',
      headers: { "Content-Type": "multipart/form-data" },
      data: {
        regno: {},
        regdate: { CDate },
        exDate: { CDate },
        studentImage: { photoSrc },
        StudentPicture: { photoSrc },
        StudentFirstName: [formDeta.StudentFirstName],
        StudentLastName: [formDeta.StudentLastName],
        StudentEmail: [formDeta.StudentEmail],
        StudentBForm: [formDeta.StudentBForm],
        StudentDob: [formDeta.StudentDob],
        StudentGender: [formDeta.StudentGender],
        StudentEntryTest: [formDeta.StudentEntryTest],
        StudentEntryTestDate: [formDeta.StudentEntryTestDate],
        StudentClassPlan: [formDeta.StudentClassPlan],
        StudentSession: [formDeta.StudentSession],

        StudentInfomation: [formDeta.StudentInfomation],
        StudentAddmissionOfficerReviews: [formDeta.StudentAddmissionOfficerReviews],
        GuardianFirstName: [formDeta.GuardianFirstName],
        GuardianLastName: [formDeta.GuardianLastName],
        GuardianEmail: [formDeta.GuardianEmail],
        GuardianCNIC: [formDeta.GuardianCNIC],
        GuardianPhone: [formDeta.GuardianPhone],
        GuardianOccupation: [formDeta.GuardianOccupation],
        GuardianAddress: [formDeta.GuardianAddress],
        Guardiancity: [formDeta.Guardiancity],
        Guardiancitiesarea: [formDeta.Guardiancitiesarea],
        guardianRelation: [formDeta.guardianRelation],


        Secondguardianfirstname: [formDeta.Secondguardianfirstname],
        SecondguardianLastName: [formDeta.SecondguardianLastName],
        SecondguardianEmail: [formDeta.SecondguardianEmail],
        SecondguardianCNIC: [formDeta.SecondguardianCNIC],
        SecondguardianPhone: [formDeta.SecondguardianPhone],
        SecondguardianOccupation: [formDeta.SecondguardianOccupation],
        SecondguardianAddress: [formDeta.SecondguardianAddress],
        Secondguardiancities: [formDeta.Secondguardiancities],
        Secondguardiancitiesarea: [formDeta.Secondguardiancitiesarea],
        SecondguardianRelation: [formDeta.SecondguardianRelation],
        SecondAddmissionOfficerReviews: [formDeta.SecondAddmissionOfficerReviews],

        BrachId: [formDeta.BranchId],
        Brachcity: [formDeta.Branchcity],
        BrachArea: [formDeta.BranchArea],
        AddmissionOfficerReviews: [formDeta.AddmissionOfficerReviews],



        // Medical info

        diseasename: [formDeta.diseasename],
        medicalcondition: [formDeta.medicalcondition],
        emergencynumber: [formDeta.emergencynumber],
        mentalhealth: [formDeta.mentalhealth],

        // Upload files
        UploadFiles: [formDeta.UploadFiles],
        Studentbform: [formDeta.Studentbform],
        pcnicFront: [formDeta.pcnicFront],
        pcnicBack: [formDeta.pcnicBack],
        leavingcertificate: [formDeta.leavingcertificate],
        birthcertificate: [formDeta.birthcertificate],

        registrationid: [formDeta.registrationid],
      },


    }).then(function (response) {
      console.log(response);
    }).catch(function (error) {
      console.log(error);
    });
  }





  return (
    <>

      <ol className="c-progress-steps">
        <li className={page === 0 ? "c-progress-steps__step current" : page >= 0 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Student information</span></li>
        <li className={page === 1 ? "c-progress-steps__step current" : page >= 1 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Parent information</span></li>
        <li className={page === 2 ? "c-progress-steps__step current" : page >= 2 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Acadamic information</span></li>
        <li className={page === 3 ? "c-progress-steps__step current" : page >= 3 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Fees collection</span></li>
        <li className={page === 4 ? "c-progress-steps__step current" : page >= 4 ? "c-progress-steps__step green" : "c-progress-steps__step done"}><span>Preview page</span></li>
      </ol>

      {DisplaySteps()}

      <div className={page === 4 ? "btn-absulote" : "btn-floating"}>
        <button className="btn btn-normal" disabled={page === 0} onClick={() => { setPage((currPage) => currPage - 1) }}>Preview</button>
        {page === StepsTitles.length - 1 ? <button className="btn btn-normal" onClick={Submitform}>submit</button> : <button className="btn btn-normal"
          onClick={() => {
            if (page === StepsTitles.length - 1) {
              Submitform({});
              setFormDeta(formDeta);
            } else {
              setPage((currPage) => currPage + 1)
            }
          }
          }
        >{page === StepsTitles.length - 1 ? "" : "Next"}</button>}


      </div>



    </>
  )
}
export default ProForm;