
import React, { Component } from 'react';

class NextStepTesting extends Component {
    
    constructor() {
		super();
		this.state = {
			colors: {
				red: true,
				green: false,
				blue: true,
				yellow: false,
				cyan: false,
			},
		};
	}

	handleClick = (event) => {
		const { name, checked } = event.target;

		this.setState((prevState) => {
			const colors = prevState.colors;
			colors[name] = checked;
			return colors;
		});
	};
    
    render() {
        const favColors = Object.keys(this.state.colors)
			.filter((key) => this.state.colors[key])
			.join(", ");
        return (
            <div className="App">
				<header className="header">
					<h1>Handling Multiple Checkboxes</h1>
				</header>

				<main>
					<div>
						<label>Choose your favourite colors</label>
						<div>
							<input checked={this.state.colors.red} onChange={this.handleClick} type="checkbox" name="red" /> Red
						</div>
						<div>
							<input checked={this.state.colors.blue} onChange={this.handleClick} type="checkbox" name="blue" /> Blue
						</div>
						<div>
							<input checked={this.state.colors.green} onChange={this.handleClick} type="checkbox" name="green" /> Green
						</div>
						<div>
							<input checked={this.state.colors.yellow} onChange={this.handleClick} type="checkbox" name="yellow" /> Yellow
						</div>
						<div>
							<input checked={this.state.colors.cyan} onChange={this.handleClick} type="checkbox" name="cyan" /> Cyan
						</div>
					</div>
					<p> Your favourite colors are: {favColors}</p>
				</main>
			</div>
        );
    }
}



export default NextStepTesting;