import React, { useState } from 'react'
// import { Link } from "react-router-dom";
import AgGrid from "../Tables";
import NumberFormat from "react-number-format";
// import axios from 'axios';
import Input from "../../Componets/widgets/ValidationInputs/input";
import Selectors from "../../Componets/widgets/ValidationInputs/Selector";
import Textarea from "../../Componets/widgets/ValidationInputs/Textarea";
import Camra from '../../Componets/widgets/camra';

const FormPage = ({ formDeta, setFormDeta, photoSrc, setSnapcapture, Snapcapture, setPhotoSrc, cities }) => {
  const [stest, setStest] = useState('');
  //// Model 
  const [modal, setModal] = useState(false);

  const toggleModal = () => {
    setModal(!modal);
  };

  if (modal) {
    document.body.classList.add('active-modal')
  } else {
    document.body.classList.remove('active-modal')
  }

  return (
    <>

      <div className="heading mb20">
        <h4><img src={process.env.PUBLIC_URL + '/images/student.png'} alt="student" /><b>New Admission</b></h4>
      </div>

      <div className="flex">
        <div className="columns-w8">
          <div className="flex form_desgin">
            <div className='custom-field idGenerate'>
              <label htmlFor={formDeta.regno}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>Reg.id</h6></label>
              <NumberFormat
                onChange={(event) => setFormDeta({ ...formDeta, regno: event.target.value })}
                value={formDeta.regno}
                id={formDeta.regno}
                format="#############"
                mask="_"
                allowEmptyFormatting={true}
              >
              </NumberFormat>
            </div>


            <Input

              className="idGenerate"
              onChange={(event) => setFormDeta({ ...formDeta, regdate: event.target.value })}
              value={formDeta.regdate}
              fieldname="Reg.date"
              type="date"
              icon="fa-calendar"
            />
            <Input

              onChange={(event) => setFormDeta({ ...formDeta, exDate: event.target.value })}
              value={formDeta.exDate}
              fieldname="Exp.date"
              type="date"
              icon="fa-calendar-o"
            />
            <button onClick={toggleModal} className="uploadfiles btn-normal" to="/">
              <i className="fa fa-cloud-upload"></i><span>add Registered Student</span>
            </button>

            <div className="heading ">
              <h5>Student information</h5>
            </div>
            <Input
              onChange={(event) => setFormDeta({ ...formDeta, StudentFirstName: event.target.value })}
              value={formDeta.StudentFirstName}
              fieldname="Student First Name"
              icon="fa-user-o"
            />
            <Input
              onChange={(event) => setFormDeta({ ...formDeta, StudentLastName: event.target.value })}
              value={formDeta.StudentLastName}
              fieldname="Student last Name"
              icon="fa-user-o"
            />
            <Input
              onChange={(event) => setFormDeta({ ...formDeta, StudentDob: event.target.value })}
              value={formDeta.StudentDob}
              fieldname="Date of Birth"
              icon="fa-user-o"
              type="date"
            />
            <Input
              onChange={(event) => setFormDeta({ ...formDeta, StudentEmail: event.target.value })}
              value={formDeta.StudentEmail}
              fieldname="Student Email@"
              icon="fa-envelope-o"
            />

            <div className='custom-field idGenerate'>
              <label htmlFor={formDeta.StudentBForm}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>Student CNIC/B-form</h6></label>
              <NumberFormat
                onChange={(event) => setFormDeta({ ...formDeta, StudentBForm: event.target.value })}
                value={formDeta.StudentBForm}
                id={formDeta.StudentBForm}
                format="#####-#######-#"
                mask="_"
                allowEmptyFormatting={true}
              >
              </NumberFormat>
            </div>
            <Selectors
              icon="fa-snowflake-o" // sessions
              fieldname="Sessions"
              value={formDeta.StudentSession}
              arryvalue={cities}
              onChange={(event) => setFormDeta({ ...formDeta, StudentSession: event.target.value })}
            />

            <Selectors
              icon="fa-snowflake-o" // sessions
              fieldname="Religion"
              value={formDeta.StudentReligion}
              arryvalue={cities}
              onChange={(event) => setFormDeta({ ...formDeta, StudentReligion: event.target.value })}
            />
            <Selectors
              icon="fa-snowflake-o" // sessions
              fieldname="Language"
              value={formDeta.StudentLanguage}
              arryvalue={cities}
              onChange={(event) => setFormDeta({ ...formDeta, StudentLanguage: event.target.value })}
            />
            <Selectors
              icon="fa-snowflake-o" // sessions
              fieldname="House"
              value={formDeta.StudentGrouping}
              arryvalue={cities}
              onChange={(event) => setFormDeta({ ...formDeta, StudentGrouping: event.target.value })}
            />




            <div className="flex">
              {stest === 1 && (<>
                <Selectors
                  arryvalue={cities}
                  icon="fa-superscript"  //test type  
                  fieldname="test type"
                  value={formDeta.StudentEntryTestType}
                  onChange={(event) => setFormDeta({ ...formDeta, StudentEntryTestType: event.target.value })}
                />
                <Input

                  onChange={(event) => setFormDeta({ ...formDeta, StudentEntryTestDate: event.target.value })}
                  value={formDeta.StudentEntryTestDate}
                  fieldname="Entry Test Date"
                  type="date"
                  icon="fa-calendar-o"
                /></>
              )
              }
              {stest === 2 && ""}


            </div>
            <div className="heading ">
              <h5>Student Medical Condition</h5>
            </div>
            <Input

              onChange={(event) => setFormDeta({ ...formDeta, diseasename: event.target.value })}
              value={formDeta.diseasename}
              fieldname="Disease Name"
              type="text"
              icon="fa-calendar"
            />
            <Input

              onChange={(event) => setFormDeta({ ...formDeta, medicalcondition: event.target.value })}
              value={formDeta.medicalcondition}
              fieldname="Medical Condition"
              type="text"
              icon="fa-calendar-o"
            />
            <div className='custom-field idGenerate'>
              <label htmlFor={formDeta.emergencynumber}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>Emergency Number</h6></label>
              <NumberFormat
                onChange={(event) => setFormDeta({ ...formDeta, emergencynumber: event.target.value })}
                value={formDeta.emergencynumber}
                id={formDeta.emergencynumber}
                format="####-#######"
                mask="_"
                allowEmptyFormatting={true}
              >
              </NumberFormat>
            </div>
            <Input

              onChange={(event) => setFormDeta({ ...formDeta, mentalhealth: event.target.value })}
              value={formDeta.mentalhealth}
              fieldname="Mental Health"
              type="text"
              icon="fa-calendar-o"
            />
            <div className="flex">
              <Textarea
                onChange={(event) => setFormDeta({ ...formDeta, StudentAddmissionOfficerReviews: event.target.value })}
                value={formDeta.StudentAddmissionOfficerReviews}
                fieldname="Remarks"
                placeholder="Remarks"
                icon="fa-comments-o"
              />
            </div>
          </div>
        </div>
        <div className="columns lst">
          {
            photoSrc && (
              <div className='top-right'>
                <img className="photo_snap" src={photoSrc} alt="img here" />
              </div>
            )
          }
          <div className="element_pr">
            <Camra
              photoSrc={photoSrc}
              setSnapcapture={setSnapcapture}
              Snapcapture={Snapcapture}
              setPhotoSrc={setPhotoSrc}
            />
            <div className="flex">
              <div className="selections mb20">
                <h6>Gender *</h6>
                <div className="flex">
                  <div className="radio_btns">
                    <Input

                      cls="radio-box"
                      onChange={(event) => setFormDeta({ ...formDeta, StudentGender: event.target.value })}
                      value="Male"
                      checked={formDeta.StudentGender === "Male"}
                      name="StudentGender"
                      fieldname='Male'
                      type="radio"
                      span={<span></span>}
                    />
                  </div>
                  <div className="radio_btns">
                    <Input
                      cls="radio-box"
                      onChange={(event) => setFormDeta({ ...formDeta, StudentGender: event.target.value })}
                      value="Female"
                      name="StudentGender"
                      checked={formDeta.StudentGender === "Female"}
                      fieldname='Female'
                      type="radio"
                      span={<span></span>}
                    />
                  </div>
                </div>
              </div>
              <div className="selections mb20">
                <h6>take entry test *</h6>
                <div className="flex">
                  <div className="radio_btns">
                    <Input
                      cls="radio-box"
                      onChange={(event) => setFormDeta({ ...formDeta, StudentEntryTest: event.target.value }, setStest(1))}
                      value="yes"
                      checked={formDeta.StudentEntryTest === "yes"}
                      fieldname={`yes`}
                      type="radio"
                      span={<span></span>}
                      name="testtype"
                    />
                  </div>
                  <div className="radio_btns">
                    <Input
                      cls="radio-box"
                      onChange={(event) => setFormDeta({ ...formDeta, ...stest, StudentEntryTest: event.target.value }, setStest(2))}
                      value="no"
                      checked={formDeta.StudentEntryTest === "no"}
                      fieldname={`no`}
                      type="radio"
                      span={<span></span>}
                      name="testtype"
                    />
                  </div>
                </div>
              </div>
              <div className="full-width parentOfupload">
                <Input
                  cls=" btn-normal"
                  onChange={(event) => setFormDeta({ ...formDeta, Bformupload: event.target.files[0] })}
                  value={formDeta.Bformupload}
                  fieldname="Upload student B-Form"
                  type="file"
                  icon="fa-cloud-upload"
                />
                <Input
                  cls=" btn-normal"
                  onChange={(event) => setFormDeta({ ...formDeta, Uploadcnicfront: event.target.files[0] })}
                  value={formDeta.Uploadcnicfront}
                  fieldname="Upload Parent CNIC FrontSide"
                  type="file"
                  icon="fa-cloud-upload"
                />
                <Input
                  cls=" btn-normal"
                  onChange={(event) => setFormDeta({ ...formDeta, Uploadcnicback: event.target.files[0] })}
                  value={formDeta.Uploadcnicback}
                  fieldname="Upload Parent CNIC BackSide"
                  type="file"
                  icon="fa-cloud-upload"
                />
                <Input
                  cls=" btn-normal"
                  onChange={(event) => setFormDeta({ ...formDeta, Birthcerificate: event.target.files[0] })}
                  value={formDeta.Birthcerificate}
                  fieldname="Upload Student Birth Certificate"
                  type="file"
                  icon="fa-cloud-upload"
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <button onClick={Continue} className="btn-normal style2 ">Next</button> */}
      {modal && (
        <div className="popup_model">
          <div onClick={toggleModal} className="overlay"></div>
          <div className="modal-content">
            <AgGrid />
            <button className="close-modal" onClick={toggleModal}>
              <i className="fa fa-times" aria-hidden="true"></i>
            </button>
          </div>
        </div>
      )}
    </>
  )

}



export default FormPage;