// import Input from "./widgets/ValidationInputs/input";
export default function FileUpload({ onFileChange, fileName, ctrlName }) {
    
    return (
        <>

            <div className="btn-normal">
                <input accept="image/*" id={fileName} onClick={(e) => { e.target.value = null; console.log(e); }} onChange={onFileChange} name={ctrlName} type="file"  />
                <label htmlFor={fileName}>{fileName}</label>
            </div>

            {/* <Input
                cls=" btn-normal"
                onChange={(event) => setFormDeta({ ...formDeta, Bformupload: event.target.files[0] })}
                value={formDeta.Bformupload}
                fieldname="Upload student B-Form"
                type="file"
                icon="fa-cloud-upload"
            /> */}
        </>);
}

