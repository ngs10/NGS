const Selectors = (props) => {
    

    return (
        <>
            <div className={'custom-field selections_box' && props.cls ? "" : "custom-field selections_box" }>
                <label><i className={`fa ${props.icon}`}></i><h6 className="margin0">{props.span}{props.fieldname}</h6></label>
                <select onChange={props.onChange} value={props.value}>
                    {props.arryvalue &&
                        props.arryvalue.map((val) => <option key={val.id} value={val.value}>{val.title}</option>)
                    }
                </select>

            </div>
        </>
    );
}
export default Selectors;