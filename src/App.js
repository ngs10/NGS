import { useState } from "react";
import {
  BrowserRouter,
  Route,
  Routes,
} from "react-router-dom";

// import Routes from "./routes";
import Home from './Pages/Home';
import NoMatch from './Componets/Tabs_content/nomatch';
import CombineElement from './layouts/CombineElement';
import Statistics from './Componets/widgets/Statistics';
import Registration from './Pages/Registrations';
import Filemaneger from './Componets/widgets/Filemaneger';
import RegistraionList from "./Pages/Registrationlist";
import NewAdmission from "./Pages/newAdmission";
import Modal from "./Componets/widgets/model";
import Testing from './Pages/Testing';
import StudentsList from "./Pages/StudentsList";
import Charts from "./Componets/widgets/charts";
import Addmissionlist from "./Pages/Addmissionlist";
import Registrations from "./Pages/Registrations";
import "/node_modules/fontawesome-4.7/css/font-awesome.min.css";
import './styles/App.css';






function App() {
  const [loading, setLoading] = useState(true);
  const Loader = document.getElementById('loader');
  if(Loader){
      setTimeout(()=>{
        Loader.style.display= "none";
        setLoading(false);
      },2000);
  }
  
  return (
     

      !loading && (
        
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<CombineElement><Home/></CombineElement>}/>
              <Route path="/Registrationlist" element={<CombineElement><RegistraionList/></CombineElement>} />
              {/* <Route path="/pagetwo" element={<CombineElement><Pagetwo/></CombineElement>} /> */}
              {/* <Route path="/table" element={<CombineElement><AgGrid/></CombineElement>} /> */}
              <Route path="/Statistics" element={<CombineElement><Statistics/></CombineElement>} />
              <Route path="/Registration" element={<CombineElement><Registration/></CombineElement>} />
              <Route path="/Registrations" element={<CombineElement><Registrations/></CombineElement>} />
              <Route path="/newAdmission" element={<CombineElement><NewAdmission/></CombineElement>} />
              <Route path="/Admissionslist" element={<CombineElement><Addmissionlist/></CombineElement>} />
              <Route path="/filemaneger" element={<CombineElement><Filemaneger/></CombineElement>} />
              <Route path="/testing" element={<CombineElement><Testing/></CombineElement>} />
              <Route path="/charts" element={<CombineElement><Charts/></CombineElement>} />
              <Route path="/model" element={<Modal/>}/>
              <Route path="/student" element={<CombineElement><StudentsList/></CombineElement>} />
              <Route path='*' element={<CombineElement><NoMatch /></CombineElement>} />
            </Routes>
          </BrowserRouter>
        
      )
    
     
     

  );
}


export default App;




























/* eslint-disable react/jsx-no-undef */
// import React, {
//   Fragment,
//   Component,
//   Suspense,
//   useEffect,
//   useState,
// } from "react";
// import { BrowserRouter, HashRouter, Link, MemoryRouter, NavLink, Navigate, NavigationType, Outlet, Route, Router, Routes, UNSAFE_LocationContext, UNSAFE_NavigationContext, UNSAFE_RouteContext, createPath, createRoutesFromChildren, createSearchParams, generatePath, matchPath, matchRoutes, parsePath, renderMatches, resolvePath, unstable_HistoryRouter, useHref, useInRouterContext, useLinkClickHandler, useLocation, useMatch, useNavigate, useNavigationType, useOutlet, useOutletContext, useParams, useResolvedPath, useRoutes, useSearchParams} from "react-router-dom";
// import Routesr from "./routes";
// import { useDispatch } from "react-redux";
// import NextApp from "./NextApp";
// import "/node_modules/fontawesome-4.7/css/font-awesome.min.css";
// import 'bootstrap/dist/css/bootstrap.min.css';
// import './App.css';



// function App() {


//   return (
//     <div>
//     <Route>
//       <Switch>
//         {Routesr.map((route, index) => {
//           return (
//             <route.route
//               key={index}
//               path='/'
//               exact={route.exact}
//               component={(props) => {
//                 const Layout = route.layout;
//                 const key =
//                   `${props.match.params.slug}${props.match.params.subSlug}${props.match.params.subSubSlug}${props.match.params.varSlug}${props.match.params.keyword}` ||
//                   route.name;
//                 return (
//                   <NextApp {...props} />
//                 );
//               }}
//             />
//           );
//         })}
//       </Switch>
//     </Route>
//     </div>
    


//   );
// }

// export default App;

