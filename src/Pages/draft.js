
import React, { useState, useRef, useCallback } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Webcam from "react-webcam";
import Input from "../Componets/widgets/ValidationInputs/input";
import Selectors from "../Componets/widgets/ValidationInputs/Selector";
import Textarea from "../Componets/widgets/ValidationInputs/Textarea";
// import Modal from "../Componets/widgets/model";


function NewAdmission() {

    // eslint-disable-next-line no-unused-vars

    const [stest, setStest] = useState('yes');
    const [gender, setGender] = useState([""]);
    const [count, setCount] = useState(0);
    const [photoSrc, setphotoSrc] = useState(null);
    const [Snapcapture, setSnapcapture] = useState('Take Picture');
    const webcamRef = useRef(null);


    // let unique_id = uuid();
    let currentDate = new Date();
    // let current = currentDate.toLocaleTimeString();
    let CDate = currentDate.toLocaleDateString();
  

    const InistialValues = {
        regno: 0,
        regdate: { CDate },
        exDate: { CDate },
        studentImage: null,
        StudentFirstName: "",
        StudentLastName: "",
        StudentEmail: "",
        StudentBForm: "",
        StudentDob: "",
        StudentGender: "",
        StudentEntryTest: "",
        StudentEntryTestDate: "",
        StudentClassPlan: "",
        StudentSession: "",
        StudentInfomation: "",
        GuardianFirstName: "",
        GuardianLastName: "",
        GuardianEmail: "",
        GuardianCNIC: "",
        GuardianPhone: "",
        GuardianOccupation: "",
        GuardianAddress: "",
        Relation: "",
        BrachId: "",
        Brachcity: "",
        BrachArea: "",
        AddmissionOfficerReviews: "",
    }
    const [FormData, setFormDeta] = useState(InistialValues);









    const capture = useCallback(() => {
        // const formData = new FormData();
        // formData.append('file', photoSrc);
        const snapSrc = webcamRef.current.getScreenshot();
        setphotoSrc(snapSrc);
        setSnapcapture('Retake');
    }, []);



    const Incriment = () => setCount(count + 1);






    function Submitform(e) {
        e.preventDefault();
        capture([0]);
        axios(
            {
                method: 'GET',
                url: 'http://172.17.0.15:8001/api/Registration/NewRegistration?LoginId=1&BrId=1',
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'POST,PATCH,OPTIONS'
                },
                statusCode: 200,
                body: JSON.stringify(),

            }
        ).then(function (response) {
            console.log(response);
        }).catch(function (error) {
            console.log(error);
        });
    }


    function Onhendler(e) {
        setFormDeta(e.target.value);
    }








    return (
        <form className="form_style" onSubmit={Submitform}>
            <div className="heading mb20">
                <h4><img src={process.env.PUBLIC_URL + '/images/student.png'} alt="student" /><b>Registration </b></h4>
            </div>

            <div className="row">
                <div className="col-9">
                    <div className="flex form_desgin">
                        <Input
                            className="idGenerate disabled"
                            fieldname="Reg.id"
                            value={FormData.regno + `${count}`}
                            disabled={true}
                            type="number"
                            icon="fa-hashtag"
                        />
                        <Input
                            className="idGenerate"
                            onChange={Onhendler}
                            value={FormData.regdate}
                            fieldname="Reg.date"
                            type="date"
                            icon="fa-calendar"
                        />
                        <Input
                            onChange={Onhendler}
                            value={FormData.exDate}
                            fieldname="Exp.date"
                            type="date"
                            icon="fa-calendar-o"
                        />
                        <Link className="uploadfiles btn-normal" to="/">
                            <i className="fa fa-cloud-upload"></i><span>add Registered Student</span>
                        </Link>

                        <div className="heading ">
                            <h5>Student information</h5>
                        </div>
                        <Input
                            onChange={Onhendler}
                            value={FormData.StudentFirstName}
                            fieldname="Student First Name"
                            icon="fa-user-o"
                        />
                        <Input

                            onChange={Onhendler}
                            value={FormData.StudentLastName}
                            fieldname="Student last Name"
                            icon="fa-user-o"
                        />
                        <Input

                            onChange={Onhendler}
                            value={FormData.StudentEmail}
                            fieldname="Student Email@"
                            icon="fa-envelope-o"
                        />
                        <Input

                            onChange={Onhendler}
                            value={FormData.StudentBForm}
                            fieldname="Student CNIC/B-form"
                            icon="fa-id-card-o"
                        />


                        <Selectors
                            icon="fa-superscript"  //test type  
                            fieldname="test type"

                        />
                        <Input
                            onChange={Onhendler}
                            value={FormData.StudentDob}
                            fieldname="Entry Test Date"
                            type="date"
                            icon="fa-calendar-o"
                        />
                        <Selectors
                            icon="fa-briefcase" // Class PLan
                            fieldname="Class Plan"

                        />
                        <Selectors
                            icon="fa-snowflake-o" // sessions
                            fieldname="Sessions"

                        />

                        <div className="heading ">
                            <h5>Parent/Guardian information</h5>
                        </div>
                        <Input
                            onChange={Onhendler}
                            value={FormData.GuardianName}
                            fieldname="Guardian Name"
                            icon="fa-envelope-o"
                        />
                        <Input
                            onChange={Onhendler}
                            value={FormData.GuardianEmail}
                            fieldname="Email@"
                            icon="fa-envelope-o"
                        />
                        <Input
                            onChange={Onhendler}
                            value={FormData.GuardianCNIC}
                            fieldname="CNIC no"
                            icon="fa-id-card-o"
                        />
                        <Input
                            onChange={Onhendler}
                            value={FormData.GuardianPhone}
                            fieldname="Phone no"
                            icon="fa-phone"
                        />
                        <Input
                            onChange={Onhendler}
                            value={FormData.GuardianOccupation}
                            fieldname="Occupation"
                            icon="fa-globe"
                        />
                        <Selectors

                            fieldname="Relationship"
                            icon="fa-compress"

                        />
                        <Selectors

                            fieldname="Branch City"
                            icon="fa-compress"

                        />
                        <Selectors

                            fieldname="Branch area"
                            icon="fa-compress"

                        />
                        <div className="flex">
                            <Textarea
                                onChange={Onhendler}
                                value={FormData.GuardianAddress}
                                // fieldname="Address"
                                placeholder="Address"
                                icon="fa-map-marker"
                            />
                            <Textarea
                                onChange={Onhendler}
                                value={FormData.AddmissionOfficerReviews}
                                // fieldname="Remarks"
                                placeholder="Remarks"
                                icon="fa-comments-o"
                            />
                        </div>
                    </div>
                </div>
                <div className="col-3">
                    <div className="element_pr">
                        <div className="webCam">
                            <Webcam
                                audio={false}
                                ref={webcamRef}
                                screenshotFormat="image/jpeg"
                            />
                            <button className="btn_capture" onClick={capture}><i className="fa fa-camera" aria-hidden="true"></i>{Snapcapture}</button>

                            {photoSrc && (
                                <img className="photo_snap" src={photoSrc} alt="img here" />
                            )}
                        </div>
                        <div className="flex">
                            <div className="selections mb20">
                                <h6>Gender *</h6>
                                <div className="flex">
                                    <div className="radio_btns">
                                        <Input
                                            cls="radio-box"
                                            onChange={(event) => setGender(event.target.value)}
                                            checked={gender === 'male'}
                                            value="male"
                                            fieldname={`Male`}
                                            type="radio"
                                            span={<span></span>}
                                            name="gender"
                                        />
                                    </div>
                                    <div className="radio_btns">
                                        <Input
                                            cls="radio-box"
                                            onChange={(event) => setGender(event.target.value)}
                                            checked={gender === 'female'}
                                            value="female"
                                            fieldname={`Female`}
                                            type="radio"
                                            span={<span></span>}
                                            name="gender"

                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="selections mb20">
                                <h6>take entry test *</h6>
                                <div className="flex">
                                    <div className="radio_btns">
                                        <Input
                                            cls="radio-box"
                                            onChange={(e) => setStest(e.target.value)}
                                            value="yes"
                                            checked={stest === 'yes'}
                                            fieldname={`yes`}
                                            type="radio"
                                            span={<span></span>}
                                            name="abc"
                                        />
                                    </div>
                                    <div className="radio_btns">
                                        <Input
                                            cls="radio-box"
                                            onChange={(e) => setStest(e.target.value)}
                                            value="No"
                                            checked={stest === 'no'}
                                            fieldname={`No`}
                                            type="radio"
                                            span={<span></span>}
                                            name="abc"

                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="full-width parentOfupload">
                                <Input
                                    cls=" btn-normal"
                                    onChange={Onhendler}
                                    value={FormData.Bformupload}
                                    fieldname="Upload student B-Form"
                                    type="file"
                                    icon="fa-cloud-upload"
                                />
                                <Input
                                    cls=" btn-normal"
                                    onChange={Onhendler}
                                    value={FormData.Uploadcnicfront}
                                    fieldname="Upload Parent CNIC FrontSide"
                                    type="file"
                                    icon="fa-cloud-upload"
                                />
                                <Input
                                    cls=" btn-normal"
                                    onChange={Onhendler}
                                    value={FormData.Uploadcnicback}
                                    fieldname="Upload Parent CNIC BackSide"
                                    type="file"
                                    icon="fa-cloud-upload"
                                />
                                <Input
                                    cls=" btn-normal"
                                    onChange={Onhendler}
                                    value={FormData.Birthcerificate}
                                    fieldname="Upload Student Birth Certificate"
                                    type="file"
                                    icon="fa-cloud-upload"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* <button className="btn-normal" type="submit">Reset</button> */}
            <button className="btn-normal" onClick={Incriment}>submit</button>
            {/* <button className="btn-normal style2 rtl" type="submit">Next</button> */}

        </form>
    );
}
export default NewAdmission;