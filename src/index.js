import React from 'react'
import ReactDOM from 'react-dom/client'
// import Store from "./redux/store";
// import { Provider } from "react-redux";
import App from './App'
import "./styles/typography.css";

// Store.subscribe(() => console.log(Store.getState()));



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  
    <div className='wrapper'>
      <App />
    </div>
  
)