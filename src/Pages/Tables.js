/* eslint-disable array-callback-return */


import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';

const AgGrid = () => {


    const columnDefs = [

        { field: 'id', filter: 'agNumberColumnFilter', suppressMenu: false ,
        cellRenderer: 'agGroupCellRenderer',
        cellRendererParams: {
            checkbox: true,
        },
    },
        { field: 'title', filter: 'agTextColumnFilter', suppressMenu: false },
        { field: 'body', filter: "agTextColumnFilter", suppressMenu: false, },

    ];
    const defaultColDef = {

        sortable: true,
        editable: true,
        flex: 1,
        filter: false,
        floatingFilter: true,
  
        
        // rowSelection: 'multiple',
        // groupSelectsChildren: true,
        // suppressRowClickSelection: true,
        // suppressAggFuncInHeader: true,
    };

    const onGridReady = (params) => {
        fetch('https://jsonplaceholder.typicode.com/posts').then(resp => resp.json())
            .then(resp => {
                console.log(resp)
                params.api.applyTransaction({ add: resp })
            })

    };
    return (


        <AgGridReact
            className="ag-theme-alpine h100"
            columnDefs={columnDefs}
            defaultColDef={defaultColDef}
            onGridReady={onGridReady}
        />


    );
}
export default AgGrid;