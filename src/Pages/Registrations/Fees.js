
import React from 'react';
import Input from '../../Componets/widgets/ValidationInputs/input';
import Selectors from '../../Componets/widgets/ValidationInputs/Selector';

function Fees({formDeta,setFormDeta,cities}) {
    return (

        <div className='Fees_grid'>
            <div className='flex form_desgin'>
                <div className='column'>
                    <Selectors
                         icon="fa-snowflake-o" // Select Template for fees
                         fieldname="Fees Template"
                         value={formDeta.StudentReligion}
                         arryvalue={cities}
                         onChange={(event) => setFormDeta({ ...formDeta, StudentReligion: event.target.value })}
                    />
                <div className='add_model_head flex'>
                    <Selectors
                         icon="fa-snowflake-o" // Monthly fees
                         fieldname="Fees Head"
                         value={formDeta.StudentReligion}
                         arryvalue={cities}
                         onChange={(event) => setFormDeta({ ...formDeta, StudentReligion: event.target.value })}
                    />
                    <button type='button' className='btn-normal'>add head</button>
                </div>
                </div>
            </div>
            <table className='grid_by_table'>
                <thead>
                    <tr>
                        <th>Head ID</th>
                        <th>Head Name</th>
                        <th>Fees Type</th>
                        <th>Default Fees</th>
                        <th>Discount</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Monthly Fee</td>
                        <td>Yearly</td>
                        <td>
                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, regdate: event.target.value })}
                                value={formDeta.regdate}
                                type="number"
                            />
                        </td>
                        <td>
                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, regdate: event.target.value })}
                                value={formDeta.regdate}
                                type="number"
                            />
                        </td>
                        <td><button className='remove_item style-2'><i className='fa fa-trash-o'></i></button></td>
                    </tr>
                </tbody>
            </table>
        </div>

    );
}

export default Fees;