import React from "react";
import { Route} from "react-router-dom";



// import Home from './Pages/Home';

// import Pageone from './Tabs_content/Pageone';
// import Pagetwo from './Tabs_content/Pagestwo';

// import Tables from './Componets/widgets/Tables';
// import Statistics from './Componets/widgets/Statistics';
// import Registration from './Pages/Registration';
// import Filemaneger from './Componets/widgets/Filemaneger';
// import Testing from './Pages/Testing';


import Layout from './layouts/CombineElement';
// import AuthorizationLayout from "./layout/AuthorizationLayout";

const Empty = React.lazy(() => import("./Tabs_content/nomatch"));

const Home = React.lazy(()=> import("./Pages/Registration"));
const Routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    route: Route,
    layout: Layout,
    exact: true,
  },
  // {
  //   path: "/login",
  //   name: "Login",
  //   component: Login,
  //   route: Route,
  //   layout: Empty,
  //   exact: true,
  // },
  // {
  //   path: "/signup",
  //   name: "Signup",
  //   component: Signup,
  //   route: Route,
  //   layout: Empty,
  // },
  {
    path: "/404",
    name: "404",
    component: Empty,
    route: Route,
    layout: Layout,
  },
  {
    path: "*",
    name: "NotFound",
    component: Empty,
    route: Route,
    layout: Empty,
  },
];

export default Routes;



// <BrowserRouter>
    //   <Routes>
    //     <Route index element={<CombineElement><Home/></CombineElement>}/>
    //     <Route path="/pageone" element={<CombineElement><Pageone /></CombineElement>} />
    //     <Route path="/pagetwo" element={<CombineElement><Pagetwo/></CombineElement>} />
    //     <Route path="/table" element={<CombineElement><Tables/></CombineElement>} />
    //     <Route path="/Statistics" element={<CombineElement><Statistics/></CombineElement>} />
    //     <Route path="/Registration" element={<CombineElement><Registration/></CombineElement>} />
    //     <Route path="/filemaneger" element={<CombineElement><Filemaneger/></CombineElement>} />
    //     <Route path="/testing" element={<CombineElement><Testing/></CombineElement>} />
    //     <Route path='*' element={<CombineElement><NoMatch /></CombineElement>} />
    //   </Routes>
    // </BrowserRouter>