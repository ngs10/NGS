import AgGrid from "../Tables";
import Input from "../../Componets/widgets/ValidationInputs/input";
import Selectors from "../../Componets/widgets/ValidationInputs/Selector";
import Textarea from "../../Componets/widgets/ValidationInputs/Textarea";
import NumberFormat from "react-number-format";
import Camra from "../../Componets/widgets/camra";
import FileUpload from "../../Componets/uploadFile";
// import Modal from "../Componets/widgets/model";

function Registraion({GetDeta,formDeta,setFormDeta,Snapcapture,setSnapcapture,photoSrc,setPhotoSrc,radioHandler,setStest,stest,modal,cities,toggleModal,ImageRemover,srcSBF,srcPCNICF,srcPCNICB,srcLeavingCertificate,srcbirthCertificate,displayImg,onFileChange}) {

    console.log(GetDeta);
    return (
        <>
            <div className="form_style" >
                <div className="heading mb20">
                    <h4><img src={process.env.PUBLIC_URL + '/images/student.png'} alt="student" /><b>New Registraion</b></h4>
                </div>

                <div className="flex">
                    <div className="columns-w8">
                        <div className="flex form_desgin">
                            <div className="flex">
                                <div className='custom-field idGenerate'>
                                    <label htmlFor={formDeta.regno}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>Reg.No</h6></label>
                                    <NumberFormat
                                        onChange={(event) => setFormDeta({ ...formDeta, regno: event.target.value })}
                                        value={GetDeta}
                                        id={formDeta.regno}
                                        format="#############"
                                        mask=""
                                        allowEmptyFormatting={true}
                                    >
                                    </NumberFormat>
                                </div>
                                <Input
                                    className="idGenerate"
                                    onChange={(event) => setFormDeta({ ...formDeta, regdate: event.target.value })}
                                    value={formDeta.regdate}
                                    fieldname="Reg.date"
                                    type="date"
                                    icon="fa-calendar"
                                />
                                <Input
                                    onChange={(event) => setFormDeta({ ...formDeta, exDate: event.target.value })}
                                    value={formDeta.exDate}
                                    fieldname="Exp.date"
                                    type="date"
                                    icon="fa-calendar-o"
                                />

                                <button onClick={toggleModal} className=" btn-normal uploadfiles">
                                    <i className="fa fa-cloud-upload"></i><span>Already exists Parent</span>
                                </button>
                                

                            </div>
                            
                            <div className="heading ">
                                <h5>Student information</h5>
                            </div>
                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, StudentFirstName: event.target.value })}
                                value={formDeta.StudentFirstName}
                                fieldname="Student First Name"
                                icon="fa-user-o"
                            />
                            <Input

                                onChange={(event) => setFormDeta({ ...formDeta, StudentLastName: event.target.value })}
                                value={formDeta.StudentLastName}
                                fieldname="Student last Name"
                                icon="fa-user-o"
                            />
                            {/* validatePhone,validatePassword, */}
                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, StudentEmail: event.target.value }) }
                                value={formDeta.StudentEmail}
                                fieldname="Student Email@"
                                icon="fa-envelope-o"
                            />
                            <div className='custom-field idGenerate'>
                                <label htmlFor={formDeta.StudentBForm}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>Student CNIC/B-form</h6></label>
                                <NumberFormat
                                    onChange={(event) => setFormDeta({ ...formDeta, StudentBForm: event.target.value })}
                                    value={formDeta.StudentBForm}
                                    id={formDeta.StudentBForm}
                                    format="#####-#######-#"
                                    mask="_"
                                    allowEmptyFormatting={true}
                                >
                                </NumberFormat>
                            </div>
                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, StudentDob: event.target.value })}
                                value={formDeta.StudentDob}
                                fieldname="Date of Birth"
                                icon="fa-user-o"
                                type="date"
                            />


                            <Selectors
                                icon="fa-snowflake-o" // sessions
                                fieldname="Sessions"
                                value={formDeta.StudentSession}
                                arryvalue={cities}
                                onChange={(event) => setFormDeta({ ...formDeta, StudentSession: event.target.value })}
                            />
                            <Selectors
                                icon="fa-briefcase" // Class PLan
                                fieldname="Class Plan"
                                value={formDeta.StudentClassPlan}
                                arryvalue={cities}
                                onChange={(event) => setFormDeta({ ...formDeta, StudentClassPlan: event.target.value })}
                            />
                            <div className="flex">
                                {stest === 1 && (<>
                                    <Selectors
                                        arryvalue={cities}
                                        icon="fa-superscript"  //test type  
                                        fieldname="test type"
                                        value={formDeta.StudentEntryTestType}
                                        onChange={(event) => setFormDeta({ ...formDeta, StudentEntryTestType: event.target.value })}
                                    />
                                    <Input
                                        onChange={(event) => setFormDeta({ ...formDeta, StudentEntryTestDate: event.target.value })}
                                        value={formDeta.StudentEntryTestDate}
                                        fieldname="Entry Test Date"
                                        type="date"
                                        icon="fa-calendar-o"
                                    /></>
                                )
                                }
                                {stest === 2 && ""}


                            </div>
                            <div className="heading ">
                                <h5>Primary Guardian information </h5>
                            </div>
                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, GuardianName: event.target.value })}
                                value={formDeta.GuardianName}
                                fieldname="Guardian Name"
                                icon="fa-user-o"
                            />

                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, GuardianEmail: event.target.value })}
                                value={formDeta.GuardianEmail}
                                fieldname="Email@"
                                icon="fa-envelope-o"
                            />
                            <div className='custom-field idGenerate'>
                                <label htmlFor={formDeta.GuardianCNIC}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>CNIC no</h6></label>
                                <NumberFormat
                                    onChange={(event) => setFormDeta({ ...formDeta, GuardianCNIC: event.target.value })}
                                    value={formDeta.GuardianCNIC}
                                    id={formDeta.GuardianCNIC}
                                    format="#####-#######-#"
                                    mask="_"
                                    allowEmptyFormatting={true}
                                >
                                </NumberFormat>
                            </div>

                            <div className='custom-field idGenerate'>
                                <label htmlFor={formDeta.GuardianPhone}><i className="fa fa-phone"></i><h6 className="margin0"><span></span>Phone no</h6></label>
                                <NumberFormat
                                    onChange={(event) => setFormDeta({ ...formDeta, GuardianPhone: event.target.value })}
                                    value={formDeta.GuardianPhone}
                                    id={formDeta.GuardianPhone}
                                    format="####-#######"
                                    mask="_"
                                    allowEmptyFormatting={true}
                                >
                                </NumberFormat>
                            </div>
                            <Input
                                onChange={(event) => setFormDeta({ ...formDeta, GuardianOccupation: event.target.value })}
                                value={formDeta.GuardianOccupation}
                                fieldname="Guardian Occupation"
                                type="text"
                                icon="fa-briefcase"
                            />

                            <Selectors
                                fieldname="Relationship"
                                icon="fa-compress"
                                arryvalue={cities}
                                value={formDeta.GuardianRelation}
                                onChange={(event) => setFormDeta({ ...formDeta, GuardianRelation: event.target.value })}

                            />
                            <Selectors
                                arryvalue={cities}
                                value={formDeta.cities}
                                onChange={(event) => setFormDeta({ ...formDeta, cities: event.target.value })}
                                fieldname="City"
                                icon="fa-compress"

                            />
                            <Selectors
                                arryvalue={cities}
                                value={formDeta.citiesarea}
                                onChange={(event) => setFormDeta({ ...formDeta, citiesarea: event.target.value })}
                                fieldname="area"
                                icon="fa-compress"

                            />
                            <div className="flex">
                                <Textarea
                                    onChange={(event) => setFormDeta({ ...formDeta, GuardianAddress: event.target.value })}
                                    value={formDeta.GuardianAddress}
                                    fieldname="Address"
                                    placeholder="Address"
                                    icon="fa-map-marker"
                                />

                                <Textarea
                                    onChange={(event) => setFormDeta({ ...formDeta, AddmissionOfficerReviews: event.target.value })}
                                    value={formDeta.AddmissionOfficerReviews}
                                    fieldname="Remarks"
                                    placeholder="Remarks"
                                    icon="fa-comments-o"
                                />
                            </div>
                            
                        </div>
                    </div>
                    <div className="columns lst">


                        <div className="element_pr">
                            <Camra
                                setPhotoSrc={setPhotoSrc}
                                photoSrc={photoSrc}
                                setSnapcapture={setSnapcapture}
                                Snapcapture={Snapcapture}

                            />

                            <div className="flex">
                                <div className="selections mb20">
                                    <h6>Gender *</h6>
                                    <div className="flex">
                                        <div className="radio_btns">
                                            <Input
                                                cls="radio-box"
                                                onChange={(event) => setFormDeta({ ...formDeta, StudentGender: event.target.value })}
                                                value="Male"
                                                name="StudentGender"
                                                checked={formDeta.StudentGender === "Male"}
                                                fieldname='male'
                                                type="radio"
                                                span={<span></span>}
                                            />
                                        </div>
                                        <div className="radio_btns">
                                            <Input
                                                cls="radio-box"
                                                onChange={(event) => setFormDeta({ ...formDeta, StudentGender: event.target.value })}
                                                value="Female"
                                                name="StudentGender"
                                                checked={formDeta.StudentGender === "Female"}
                                                fieldname='female'
                                                type="radio"
                                                span={<span></span>}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="selections mb20">
                                    <h6>take entry test *</h6>
                                    <div className="flex">
                                        <div className="radio_btns">
                                            <Input
                                                cls="radio-box"
                                                checked={stest === 1}
                                                onClick={() => radioHandler(1)}
                                                onChange={() => setStest(1)}
                                                value="yes"
                                                fieldname={`yes`}
                                                type="radio"
                                                span={<span></span>}
                                                name="testtype"
                                            />
                                        </div>
                                        <div className="radio_btns">
                                            <Input
                                                cls="radio-box"
                                                checked={stest === 2}
                                                onClick={() => radioHandler(2)}
                                                onChange={() => setStest(2)}
                                                value="no"
                                                fieldname={`no`}
                                                type="radio"
                                                span={<span></span>}
                                                name="testtype"
                                            />
                                        </div>


                                    </div>
                                </div>
                                <div className="full-width parentOfupload">
                                    <div className="image_box">
                                        <FileUpload
                                            fileName="Student B-Form Name" ctrlName='SBF' onFileChange={onFileChange}
                                        />
                                        {srcSBF && <><img src={srcSBF} alt='Student B-Form' onClick={displayImg} onMouseEnter={displayImg} id="SBF" /><span className="fa fa-trash" onClick={() => ImageRemover(1)} ></span></>}
                                    </div>
                                    <div className="image_box">
                                        <FileUpload
                                            fileName="Parent CNIC Front Side" ctrlName='PCNICF' onFileChange={onFileChange}
                                        />
                                        {srcPCNICF && <><img onMouseEnter={displayImg} src={srcPCNICF} onClick={displayImg} alt='Parent CNIC Front Side' id="PCNICF" /><span className="fa fa-trash" onClick={() => ImageRemover(2)} ></span></>}
                                    </div>
                                    <div className="image_box">
                                        <FileUpload
                                            fileName="Parent CNIC Back Side" ctrlName='PCNICB' onFileChange={onFileChange}
                                        />
                                        {srcPCNICB && <><img onMouseEnter={displayImg} src={srcPCNICB} onClick={displayImg} alt='Parent CNIC Back Side' id="PCNICB" /><span className="fa fa-trash" onClick={() => ImageRemover(3)} ></span></>}
                                    </div>
                                    <div className="image_box">
                                        <FileUpload
                                            fileName="Birth Certificate" ctrlName='BC' onFileChange={onFileChange}
                                        />
                                        {srcbirthCertificate && <><img onMouseEnter={displayImg} onClick={displayImg} src={srcbirthCertificate} alt='Birth Certificate' id="BC" /><span className="fa fa-trash" onClick={() => ImageRemover(4)} ></span></>}
                                    </div>
                                    <div className="image_box">
                                        <FileUpload
                                            fileName="Leaving Certificate" ctrlName='LC' onFileChange={onFileChange}
                                        />
                                       {srcLeavingCertificate && <><img onMouseEnter={displayImg} onClick={displayImg} src={srcLeavingCertificate} alt='Leaving Certificate' id="LC" /><span className="fa fa-trash" onClick={() => ImageRemover(5)} ></span></>}
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {modal && (
                <div className="popup_model">
                    <div onClick={toggleModal} className="overlay"></div>
                    <div className="modal-content">
                        <AgGrid />
                        <button className="close-modal" onClick={toggleModal}>
                            <i className="fa fa-times" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            )}
        </>
    );
}
export default Registraion;