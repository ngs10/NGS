/* eslint-disable no-undef */

import React,{useState, useRef, useCallback} from 'react'
import Webcam from "react-webcam";

function Camra() {


    const [photoSrc, setphotoSrc] = useState(null);
    const [Snapcapture, setSnapcapture] = useState('Take Picture');
    const webcamRef = useRef(null);

    
    const capture = useCallback(() => {
        const snapSrc = webcamRef.current.getScreenshot();
        setphotoSrc(snapSrc);
        setSnapcapture('Retake');
    }, []);
    
    return (
        <div className="webCam">
            <Webcam
                audio={false}
                ref={webcamRef}
                screenshotFormat="image/jpeg"
            />
            <button className="btn_capture" onClick={capture}><i className="fa fa-camera" aria-hidden="true"></i>{Snapcapture}</button>

            {photoSrc && (
                <img className="photo_snap" src={photoSrc} alt="img here" />
            )}
        </div>
    );
}

export default Camra;