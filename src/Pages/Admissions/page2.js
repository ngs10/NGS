import React, { useState } from 'react'
import NumberFormat from "react-number-format";
import Input from "../../Componets/widgets/ValidationInputs/input";
import Selectors from "../../Componets/widgets/ValidationInputs/Selector";
import Textarea from "../../Componets/widgets/ValidationInputs/Textarea";
const FormPage2 = ({ formDeta, setFormDeta, cities }) => {

  const [isCollapsed, setIsCollapsed] = useState();






  return (
    <>
      <div className="heading mb20">
        <h4><img src={process.env.PUBLIC_URL + '/images/student.png'} alt="student" /><b>Parent/Guardian information</b></h4>
      </div>

      <div className="row">
        <div className="col-12">
          <div className="flex form_desgin">
            <div className="heading ">
              <h5>Primary Guardian information</h5>
            </div>
            <Input
              onChange={(event) => setFormDeta({ ...formDeta, GuardianName: event.target.value })}
              value={formDeta.GuardianName}
              fieldname="Guardian Name"
              icon="fa-user-o"
            />

            <Input
              onChange={(event) => setFormDeta({ ...formDeta, GuardianEmail: event.target.value })}
              value={formDeta.GuardianEmail}
              fieldname="Email@"
              icon="fa-envelope-o"
            />
            <div className='custom-field idGenerate'>
              <label htmlFor={formDeta.GuardianCNIC}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>CNIC no</h6></label>
              <NumberFormat
                onChange={(event) => setFormDeta({ ...formDeta, GuardianCNIC: event.target.value })}
                value={formDeta.GuardianCNIC}
                id={formDeta.GuardianCNIC}
                format="#####-#######-#"
                mask="_"
                allowEmptyFormatting={true}
              >
              </NumberFormat>
            </div>

            <div className='custom-field idGenerate'>
              <label htmlFor={formDeta.GuardianPhone}><i className="fa fa-phone"></i><h6 className="margin0"><span></span>Phone no</h6></label>
              <NumberFormat
                onChange={(event) => setFormDeta({ ...formDeta, GuardianPhone: event.target.value })}
                value={formDeta.GuardianPhone}
                id={formDeta.GuardianPhone}
                format="####-#######"
                mask="_"
                allowEmptyFormatting={true}
              >
              </NumberFormat>
            </div>
            <Selectors
              arryvalue={cities}
              value={formDeta.Guardiancity}
              onChange={(event) => setFormDeta({ ...formDeta, Guardiancity: event.target.value })}
              fieldname="City"
              icon="fa-compress"

            />
            <Selectors
              arryvalue={cities}
              value={formDeta.Guardiancitiesarea}
              onChange={(event) => setFormDeta({ ...formDeta, Guardiancitiesarea: event.target.value })}
              fieldname="Area"
              icon="fa-compress"

            />
            <Input
              onChange={(event) => setFormDeta({ ...formDeta, GuardianOccupation: event.target.value })}
              value={formDeta.GuardianOccupation}
              fieldname="Guardian Occupation"
              type="text"
              icon="fa-briefcase"
            />

            <Selectors
              fieldname="Relationship"
              icon="fa-compress"
              arryvalue={cities}
              value={formDeta.GuardianRelation}
              onChange={(event) => setFormDeta({ ...formDeta, GuardianRelation: event.target.value })}

            />

            <div className="flex">

              <Textarea
                onChange={(event) => setFormDeta({ ...formDeta, GuardianAddress: event.target.value })}
                value={formDeta.GuardianAddress}
                fieldname="Address"
                placeholder="Address"
                icon="fa-map-marker"
              />

              <Textarea
                onChange={(event) => setFormDeta({ ...formDeta, AddmissionOfficerReviewsP: event.target.value })}
                value={formDeta.AddmissionOfficerReviewsP}
                fieldname="Remarks"
                placeholder="Remarks"
                icon="fa-comments-o"
              />

            </div>


            <div className="heading ">
              <h5><button
                onClick={() => setIsCollapsed(!isCollapsed)}
              >
                <span className={isCollapsed ? 'ti-minus' : 'ti-plus'}></span> Secondary Guardian information
              </button></h5>
            </div>
            <div className={`flex ${isCollapsed ? 'active' : 'inactive'}`}
              aria-expanded={isCollapsed}
            >
              <Input

                onChange={(event) => setFormDeta({ ...formDeta, SecondguardianName: event.target.value })}
                value={formDeta.SecondguardianName}
                fieldname="Second Guardian Name"
                type="text"
                icon="fa-user-o"
              />
              <Input

                onChange={(event) => setFormDeta({ ...formDeta, SecondguardianEmail: event.target.value })}
                value={formDeta.SecondguardianEmail}
                fieldname="Second Guardian Email"
                type="text"
                icon="fa-envelope-o"
              />
              <div className='custom-field idGenerate'>
                <label htmlFor={formDeta.SecondguardianCNIC}><i className="fa fa-id-card-o"></i><h6 className="margin0"><span></span>Second Guardian CNIC</h6></label>
                <NumberFormat
                  onChange={(event) => setFormDeta({ ...formDeta, SecondguardianCNIC: event.target.value })}
                  value={formDeta.SecondguardianCNIC}
                  id={formDeta.SecondguardianCNIC}
                  format="#####-#######-#"
                  mask="_"
                  allowEmptyFormatting={true}
                >
                </NumberFormat>
              </div>
              <div className='custom-field idGenerate'>
                <label htmlFor={formDeta.SecondguardianPhone}><i className="fa fa-phone"></i><h6 className="margin0"><span></span>Second Guardian Phone No</h6></label>
                <NumberFormat
                  onChange={(event) => setFormDeta({ ...formDeta, SecondguardianPhone: event.target.value })}
                  value={formDeta.SecondguardianPhone}
                  id={formDeta.SecondguardianPhone}
                  format="####-#######"
                  mask="_"
                  allowEmptyFormatting={true}
                >
                </NumberFormat>
              </div>
              <Selectors
                arryvalue={cities}
                value={formDeta.Secondguardiancities}
                onChange={(event) => setFormDeta({ ...formDeta, Secondguardiancities: event.target.value })}
                fieldname="City"
                icon="fa-compress"

              />
              <Selectors
                arryvalue={cities}
                value={formDeta.Secondguardiancitiesarea}
                onChange={(event) => setFormDeta({ ...formDeta, Secondguardiancitiesarea: event.target.value })}
                fieldname="Area"
                icon="fa-compress"

              />
              <Input

                onChange={(event) => setFormDeta({ ...formDeta, SecondguardianOccupation: event.target.value })}
                value={formDeta.SecondguardianOccupation}
                fieldname="Second Guardian Occupation"
                type="text"
                icon="fa-briefcase"
              />

              <Selectors
                value={formDeta.SecondguardianRelation}
                arryvalue={cities}
                onChange={(event) => setFormDeta({ ...formDeta, SecondguardianRelation: event.target.value })}
                fieldname="Relationship"
                icon="fa-compress"

              />

              <div className="flex">

                <Textarea
                  onChange={(event) => setFormDeta({ ...formDeta, SecondguardianAddress: event.target.value })}
                  value={formDeta.SecondguardianAddress}
                  // fieldname="Address"
                  placeholder="Address"
                  icon="fa-map-marker"
                />

                <Textarea
                  onChange={(event) => setFormDeta({ ...formDeta, AddmissionOfficerReviewsS: event.target.value })}
                  value={formDeta.AddmissionOfficerReviewsS}
                  // fieldname="Remarks"
                  placeholder="Remarks"
                  icon="fa-comments-o"
                />

              </div>
            </div>

          </div>
        </div>

      </div>



    </>
  )

}



export default FormPage2;