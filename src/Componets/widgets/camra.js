import { useState, useRef } from "react";
import Webcam from "react-webcam";
function Camra({photoSrc, setSnapcapture,Snapcapture, setPhotoSrc }) {
   
   
    const webcamRef = useRef(null);
    const capture = () => {
        const snapSrc = webcamRef.current.getScreenshot();
        setPhotoSrc(snapSrc);
        setSnapcapture('Retake');

    };
    
    const imageMimeType = /image\/(png|jpg|jpeg)/i;
    const [file, setFile] = useState(null);
    const [fileDataURL, setFileDataURL] = useState(null);



    const changeHandler = (e) => {
        const file = e.target.files[0];
        if (!file.type.match(imageMimeType)) {
            alert("Image mime type is not valid");
            return;
        }
        setPhotoSrc(fileDataURL);
        setFile(file);

    }


    function Filesuploader() {
        let fileReader, isCancel = false;
        if (file) {
            fileReader = new FileReader();
            fileReader.onload = (e) => {
                const { result } = e.target;
                if (result && !isCancel) {
                    setFileDataURL(result);
                    setFile(file);
                }

            }
            fileReader.readAsDataURL(file);

        }

        return () => {

            isCancel = true;
            if (fileReader && fileReader.readyState === 0) {
                fileReader.abort();

            }
        }

    };
    Filesuploader();
    return (
        <>
        {photoSrc && (
            <div className='top-right'>
                <img className="photo_snap" src={photoSrc} alt="img here" />
            </div>
        )}
        <div className="webCam">
        
            <Webcam
                audio={false}
                ref={webcamRef}
                screenshotFormat="image/jpeg"
            />
            <button className="btn_capture" onClick={capture} type="button"><i className="fa fa-camera" aria-hidden="true"></i>{Snapcapture}</button>
            <div className='uploadPicture'>
                <label htmlFor='image' className='btn-normal'> Browse images </label>
                <input
                    type="file"
                    id='image'
                    accept='.png, .jpg, .jpeg'
                    onChange={changeHandler}
                />
            </div>
        </div>
        </>
    )
}
export default  Camra;